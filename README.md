<h1> 301, 404 et 410 </h1>

<h2>Impact 404</h2>

<p>L'impact des erreurs 404 sur un site n'est pas très grave heuresement, mais peut avoir des effets négatif sur le long terme. Un grand nombre de ces erreurs peut simplement freiner votre référencement et agir sur vos statistiques utilisateurs.</p> </br>
<p>Exemple: Si vous supprimez une page qui recevait des liens externes, cette page, devenue 404, ne transmettra plus la popularité qu’elle recevait à l’ensemble du site. </br>
Et si ce cas est redondant sur votre site, cela aura pour effets de ralentir les bots(ex: robot google) car ils vont perdre du temps à aller sur des pages qui n'existent pas, au lieu de crawler(collecter) et indexer des pages pertinentes.</p>

<h2> D'ou viennent les erreurs 404 </h2>

<p>En général les erreurs 404 sont du à: <ul>
                                            <li>Une migration du site web</li>
                                            <li>Des pages qui ont été supprimées</li>
                                            <li>Des pages dont l'URL a été modifiées sans qu'une redirection n'ait été effectué</li>
                                         </ul>
Une migration de site peut engendrer sur le cour terme des erreurs 404 car il faudra s'assurer en amont en que les URL du nouveau site soit les mêmes que ceux de l'ancien site. Pour palier à ce problème nous pouvons utiliser le code d'erreur 301 qui redirigera automatiquement l'utilisateur ou le bot sur la nouvelle page. En général c'est une tâche qui se fait manuellement et qui peut être assez longue en fonction du nombre d'URL à rediriger. Dans le cas des "grands sites", il est fréquent de ne rediriger que les pages avec un taux de trafic élevé. Les pages non rediriger vont donc créer des erreurs 404.</p>

<p> Voici un exemple de redirection 301 sous CS-cart: </p> </br>

<p>Une fois sur le dashboard(tableau de bord) de CS-cart, rendez-vous dans l'onglet website(site web):</p>
![redirect](/uploads/4aba9376076a60e74f7995d0846719f1/redirect.PNG)

<p> Vous pouvez maintenant rediriger l'URL source vers l'URL de destination </p>
![301](/uploads/2ce91a942c3a086855683ed51f7b074d/301.PNG)

<h2> Comment corriger les erreurs 404 </h2>
<p>Corriger ces pages est assez facile, mais souvent répétitif. Vous dedvez rediriger ces URL inexistantes vers de nouvelles pages, ou de déclarer ces pages comme définitivement supprimées grâce à un code 410.
Pour rediriger vos erreurs 404, vous allez donc devoir, pour chaque page 404, indiquer la nouvelle URL. Choisissez la page la plus proche au niveau du contenu.</p>

<h2> Outils </h2>

<h3>Google Search Console</h3>
<p> Comme d'habitude, Google s'occupe de tout. Ils nous ont mit à disposition un programme gratuit, Google Search Console(SC) anciennement appelez WebMasterTools, et qui va nous permettre de nous donner une liste détaillée de toutes les URL avec des erreurs.</p>

<p> Voici comment accéder au menu qui répértorie toutes les erreurs dans SC:</p>
![sc](/uploads/58c9a75d766a15cd0d902dd3c0e11264/sc.PNG)

<p> Liste d'erreurs 404 dans SC:</p>
![404](/uploads/f7f05d393a441b4443b2df2224db7f2f/404.PNG)

<p> Pour corriger l'erreur, cliquez sur un des liens que SC vous donne. Ensuite déplacez vous de un niveau de répertoire à la fois dans l'URL jusqu'à ce que vous trouviez quelque chose.</p> </br>
<p>Par exemple, si www.web.com/a/b/c.html vous a fourni l'erreur 404 Not Found, passez à l'adresse www.web.com/a/b/. Si vous n'obtenez rien ici (ou une erreur), allez sur www.web.com/a/. Cela devrait vous mener vers ce que vous cherchez ou au moins confirmer qu'il n'est plus disponible.</p> </br>
<p>Une fois que vous avez trouvé l'erreur vous n'avez plus qu'à rediriger cette page ou vous le souhaiter ou alors comme dit précèdemment la supprimer définitivement avec le code 410.</p>

<h3>Semrush</h3>

<p>Contrairement au programme précèdent, celui-ci est payant. Mais vas grandement vous aider dans le référencement et le SEO. Il va vous permettre par exemple de voir qu'elles sont les mots clés les plus pértinant ou encore de voir la position de votre site par rapport à vos concurrent. (voir ci-dessous): </p>
![semrsh](/uploads/2470b6e2fb150a5a123cb543e8e75c13/semrsh.PNG)

<p>Vous n'êtes biensur pas obliger d'avoir ce programme, il va simplement vous donner un bon coup de pouce pour le SEO.</p>